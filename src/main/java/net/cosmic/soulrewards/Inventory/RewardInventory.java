package net.cosmic.soulrewards.Inventory;

import net.cosmic.soulrewards.SoulRewards;
import net.cosmic.soulrewards.uitl.BetterInventory.BetterInventory;
import net.cosmic.soulrewards.uitl.FileConfig;
import net.cosmic.soulrewards.uitl.FileConfigManager.FileManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import static net.cosmic.soulrewards.uitl.BetterInventory.ItemCreator.createItem;
import static net.cosmic.soulrewards.uitl.TimeConversion.timeConversion;

public class RewardInventory {

    SoulRewards plugin = JavaPlugin.getPlugin(SoulRewards.class);


    public void menuInventory(Player player) {
        long timeUntilReward = plugin.getFileManager().getTimeUntilReward(player);

        String displayname = plugin.getMessagesManager().getRewardName();
        String lore = plugin.getMessagesManager().getLore();
        String loreIfClaimed = plugin.getMessagesManager().getLoreIfClaimed();
        String timeColor = plugin.getMessagesManager().getTimeColor();

        ItemStack normalReward = createItem(Material.CHEST_MINECART, 1, displayname, lore);

        ItemStack normalRewardEmpty = createItem(Material.MINECART, 1, displayname, loreIfClaimed, timeColor + timeConversion(timeUntilReward));

        boolean rewardAvailable = plugin.getFileManager().rewardAvailable(player);

        ItemStack[][] menuInventory = {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, rewardAvailable ? normalReward : normalRewardEmpty, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
        };

        BetterInventory menuRewards = new BetterInventory(menuInventory, 3 * 9, "Rewards", false);

        menuRewards.fillMenu().fillBackground(Material.GRAY_STAINED_GLASS_PANE).open(player);

        SoulRewards.getInstance().getMenuInventories().put(player, menuRewards);
    }
}

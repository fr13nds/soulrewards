package net.cosmic.soulrewards;

import com.earth2me.essentials.Essentials;
import net.cosmic.soulrewards.Commands.RewardsCommand;
import net.cosmic.soulrewards.Listener.onClick;
import net.cosmic.soulrewards.Listener.onClose;
import net.cosmic.soulrewards.Listener.onJoin;
import net.cosmic.soulrewards.MessagesManager.MessagesManager;
import net.cosmic.soulrewards.uitl.BetterInventory.BetterInventory;
import net.cosmic.soulrewards.uitl.ConfigReader.RewardsManager;
import net.cosmic.soulrewards.uitl.FileConfig;
import net.cosmic.soulrewards.uitl.FileConfigManager.FileManager;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public final class SoulRewards extends JavaPlugin {

    private HashMap<Player, BetterInventory> menuInventories = new HashMap<>();
    RewardsManager rewardsManager;

    private MessagesManager messagesManager;
    private static SoulRewards instance;

    private FileConfig fileConfig;
    private FileManager fileManager;

    private  Essentials essentials;

    @Override
    public void onEnable() {
        instance = this;
        // Plugin startup logic
        instance.saveDefaultConfig();
        this.fileConfig = new FileConfig("rewardsTime.yml");

        essentials =  (Essentials) getServer().getPluginManager().getPlugin("Essentials");

        fileManager = new FileManager(this);


        rewardsManager = new RewardsManager(this);
        messagesManager = new MessagesManager(this);


        listenerRegister();
        this.getCommand("rewards").setExecutor(new RewardsCommand());
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public void listenerRegister() {
        getServer().getPluginManager().registerEvents(new onClick(), this);
        getServer().getPluginManager().registerEvents(new onClose(), this);
        getServer().getPluginManager().registerEvents(new onJoin(), this);
    }


    public HashMap<Player, BetterInventory> getMenuInventories() {
        return menuInventories;
    }

    public static SoulRewards getInstance() {
        return instance;
    }


    public Essentials getEssentials() {
        return essentials;
    }

    public FileConfig getFileConfig() {
        return fileConfig;
    }

    public MessagesManager getMessagesManager() {
        return messagesManager;
    }

    public FileManager getFileManager() {
        return fileManager;
    }
}

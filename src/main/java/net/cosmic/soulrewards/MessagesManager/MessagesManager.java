package net.cosmic.soulrewards.MessagesManager;

import net.cosmic.soulrewards.SoulRewards;
import net.cosmic.soulrewards.uitl.ConfigReader.RewardsManager;
import net.cosmic.soulrewards.uitl.Object.Rewards;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.List;
import java.util.Locale;

public class MessagesManager {

    private String RewardMessage;
    private String NoPermissionMessage;
    private String NoRewardsMessage;
    private String NoPermissionToClaimMessage;

    private String Prefix;

    private String RewardName;

    private String Lore;

    private String TimeColor;

    private String LoreIfClaimed;

    private String ClaimRewardAvailable;

    private FileConfiguration config;


    private SoulRewards instance;

    public MessagesManager(SoulRewards instance) {
        config = instance.getConfig();
        this.instance = instance;

        lexer();
    }

    /**
     * Checks if the message is valid and if it is, the messages will be put into a variable
     *
     * @throws IllegalArgumentException if the message is not valid
     */
    public void lexer() throws IllegalArgumentException {

        try {
            Prefix = config.getString("prefix").replace("'", "");
            RewardMessage = config.getString("messages.RewardMessage").replace("'", "");
            NoPermissionMessage = config.getString("messages.NoPermissionCommand").replace("'", "");
            NoRewardsMessage = config.getString("messages.NoRewards").replace("'", "");
            NoPermissionToClaimMessage = config.getString("messages.NoPermissionReward").replace("'", "");
            RewardName = config.getString("gui.RewardName").replace("'", "");
            Lore = config.getString("gui.Lore").replace("'", "");
            TimeColor = config.getString("gui.TimeColor").replace("'", "");
            LoreIfClaimed = config.getString("gui.LoreIfClaimed").replace("'", "");
            ClaimRewardAvailable = config.getString("messages.ClaimRewardAvailable").replace("'", "");


        } catch (IllegalArgumentException exception) {
            exception.printStackTrace();
        }

        List<Rewards> rewardsList = RewardsManager.getRewardsManager().getRewards();
        String[] materialNames = new String[rewardsList.size()];
        String message = "";
        for (int i = 0; i < rewardsList.size(); i++) {

            if (rewardsList.get(i).getItemStack() == null) {

                materialNames[i] = rewardsList.get(i).getMoney() + " €";
            } else {

                materialNames[i] = rewardsList.get(i).getItemStack().getType().name().toLowerCase(Locale.GERMAN) + " x" + rewardsList.get(i).getItemStack().getAmount();
            }

        }

        for (int i = 0; i < materialNames.length; i++) {

            if (i == materialNames.length - 1) {
                // Last element, no need for a comma
                message = message.concat(" " + StringUtils.capitalize(materialNames[i]));
                continue;
            }
            if (i == 0) {

                message = message.concat(StringUtils.capitalize(materialNames[i]) + ",");

            } else {
                message = message.concat(" " + StringUtils.capitalize(materialNames[i]) + ",");

            }
        }

        RewardMessage = RewardMessage.replace("{rewards}", message.replace("_", " "));

    }

    public String getNoPermissionMessage() {
        return Prefix.concat(NoPermissionMessage);
    }

    public String getNoRewardsMessage() {
        return Prefix.concat(NoRewardsMessage);
    }

    public String getNoPermissionToClaimMessage() {
        return Prefix.concat(NoPermissionToClaimMessage);
    }

    public String getRewardMessage() {
        return Prefix.concat(RewardMessage);
    }

    public String getLore() {
        return Lore;
    }

    public String getRewardName() {
        return RewardName;
    }

    public String getTimeColor() {
        return TimeColor;
    }

    public String getPrefix() {
        return Prefix;
    }

    public String getLoreIfClaimed() {
        return LoreIfClaimed;
    }

    public String getClaimRewardAvailable() {
        return Prefix.concat(ClaimRewardAvailable);
    }


}

package net.cosmic.soulrewards.Commands;

import net.cosmic.soulrewards.Inventory.RewardInventory;
import net.cosmic.soulrewards.SoulRewards;
import net.cosmic.soulrewards.uitl.ConfigReader.RewardsManager;
import net.cosmic.soulrewards.uitl.FileConfigManager.FileManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

public class RewardsCommand implements CommandExecutor {

    SoulRewards plugin = JavaPlugin.getPlugin(SoulRewards.class);

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {


        if (!(commandSender instanceof Player)) {
            return false;
        }

        //Only for testing and debugging
        String noPermission = SoulRewards.getInstance().getMessagesManager().getNoPermissionMessage();
        if (strings.length != 0) {

            if (Objects.equals(strings[0], "reset")) {
                if (!commandSender.hasPermission("soulrewards.admin")) {


                    commandSender.sendMessage(noPermission);
                    return true;
                }
                plugin.getFileManager().setRewardsTimeZero((Player) commandSender);
                return true;
            } else if (Objects.equals(strings[0], "reload")) {
                if (!commandSender.hasPermission("soulrewards.admin")) {

                    commandSender.sendMessage(noPermission);
                    return true;
                }
                plugin.reloadConfig();
                commandSender.sendMessage("§cConfig reloaded");
                return true;
            }
        }

        Player player = (Player) commandSender;


        RewardInventory rewardInventory = new RewardInventory();

        rewardInventory.menuInventory(player);

        return true;
    }
}

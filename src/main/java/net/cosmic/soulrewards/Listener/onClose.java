package net.cosmic.soulrewards.Listener;

import net.cosmic.soulrewards.SoulRewards;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;


public class onClose implements Listener{

    @EventHandler
    public void onClose(InventoryCloseEvent event){

        if(SoulRewards.getInstance().getMenuInventories().get(event.getPlayer()) == null){
            return;
        }

        SoulRewards.getInstance().getMenuInventories().remove(event.getPlayer());

    }
}

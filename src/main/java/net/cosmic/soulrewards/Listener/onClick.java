package net.cosmic.soulrewards.Listener;

import net.cosmic.soulrewards.Inventory.RewardInventory;
import net.cosmic.soulrewards.MessagesManager.MessagesManager;
import net.cosmic.soulrewards.SoulRewards;
import net.cosmic.soulrewards.uitl.ConfigReader.RewardsManager;
import net.cosmic.soulrewards.uitl.FileConfigManager.FileManager;
import net.cosmic.soulrewards.uitl.MoneyManager.MoneyManager;
import net.cosmic.soulrewards.uitl.Object.Rewards;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;


public class onClick implements Listener {

    private final SoulRewards plugin = JavaPlugin.getPlugin(SoulRewards.class);

    MoneyManager moneyManager = new MoneyManager();

    MessagesManager messagesManager = SoulRewards.getInstance().getMessagesManager();

    RewardInventory rewardInventory = new RewardInventory();
    @EventHandler
    public void onClick(InventoryClickEvent event){
        Player player = (Player) event.getWhoClicked();

        if(SoulRewards.getInstance().getMenuInventories().get(player) == null){
            return;
        }

        if(event.getSlot() == 13){


            
            if(!plugin.getFileManager().rewardAvailable(player)){
                String claimed = messagesManager.getNoRewardsMessage();
                player.sendMessage(claimed);

                player.playSound(player.getLocation(), "entity.villager.no", 1, 0);
                event.setCancelled(true);
                return;
            }

            List<Rewards> rewardsList = RewardsManager.getRewardsManager().getRewards();

            for(Rewards rewards : rewardsList){
                ItemStack itemStack = rewards.getItemStack();

                if(itemStack == null){
                    moneyManager.addPurseBalance(player, rewards.getMoney());

                    continue;
                }

                player.getInventory().addItem(itemStack);
            }

            player.playSound(player.getLocation(), "entity.player.levelup", 1, 0);

           plugin.getFileManager().resetTime(player);

            String reward = messagesManager.getRewardMessage();
            player.sendMessage(reward);
            rewardInventory.menuInventory(player);
        }
        event.setCancelled(true);

    }
}

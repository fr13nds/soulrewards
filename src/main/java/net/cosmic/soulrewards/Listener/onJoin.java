package net.cosmic.soulrewards.Listener;

import net.cosmic.soulrewards.SoulRewards;
import net.cosmic.soulrewards.uitl.FileConfigManager.FileManager;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class onJoin implements Listener {

    FileManager fileManager = SoulRewards.getInstance().getFileManager();
    @EventHandler
    public void onJoin(PlayerJoinEvent event){

        if(!fileManager.playerExists(event.getPlayer())){
            fileManager.addPlayer(event.getPlayer());
        }

        if(fileManager.rewardAvailable(event.getPlayer())){
            String rewardAvailable = SoulRewards.getInstance().getMessagesManager().getClaimRewardAvailable();
            Bukkit.getScheduler().scheduleSyncDelayedTask(SoulRewards.getInstance(), new Runnable() {
                @Override
                public void run() {
                    event.getPlayer().sendMessage(rewardAvailable);
                }
            }, 60L); //20 Tick (1 Second) delay before run() is called


        }

    }
}

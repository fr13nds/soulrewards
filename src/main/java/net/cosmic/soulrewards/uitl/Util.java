package net.cosmic.soulrewards.uitl;

import org.bukkit.inventory.ItemStack;

public class Util {

    public static ItemStack[] fromMatrixToArray(ItemStack[][] matrix, int rows, int columns) {
        ItemStack[] array = new ItemStack[rows*columns];
        int i = 0;
        for(ItemStack[] row : matrix) {
            for(ItemStack item : row) {
                array[i] = item;
                i++;
            }
        }
        return array;
    }
}
package net.cosmic.soulrewards.uitl.Object;

import org.bukkit.inventory.ItemStack;

public class Rewards {

    private final ItemStack itemStack;
    private final int money;




    public Rewards(ItemStack itemStack) {
        this.itemStack = itemStack;
        this.money = 0;

    }

    public Rewards(int amount){
        this.money = amount;
        this.itemStack = null;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public int getMoney(){
        return money;
    }


}

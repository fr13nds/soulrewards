package net.cosmic.soulrewards.uitl;

import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class FileConfig extends YamlConfiguration {
    public String fileConfigName;

    public FileConfig(String fileName) {
        this.fileConfigName = "plugins" + File.separator + "SoulRewards" + File.separator + fileName;
        try {
            load(this.fileConfigName);
        } catch (IOException | InvalidConfigurationException e) {
            Bukkit.broadcastMessage("loading file");
        }
    }

    public void saveConfig() {
        try {
            save(this.fileConfigName);
        } catch (IOException e) {
            Bukkit.broadcastMessage("saving file");
        }
    }
}
package net.cosmic.soulrewards.uitl.ConfigReader;

import net.cosmic.soulrewards.SoulRewards;
import net.cosmic.soulrewards.uitl.Object.Rewards;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemFactory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RewardsManager {

    private final SoulRewards instance;

    private static RewardsManager rewardsManager;

    private final List<Rewards> rewards = new ArrayList<>();

    private final FileConfiguration config;


    public List<Rewards> getRewards() {
        return rewards;
    }

    ItemFactory itemFactory = Bukkit.getItemFactory();


    public RewardsManager(SoulRewards instance) {
        this.instance = instance;
        rewardsManager = this;
        config = instance.getConfig();

        reader();

    }

    /**
     * Check if the input of the config is correctly formatted
     * Must be in the format of "materialname {amount}" and separated by a comma
     *
     * @throws IllegalArgumentException if the input is not formatted correctly
     */
    public void reader() {
        Pattern pattern = Pattern.compile("(.+?) \\{(\\d+)\\}");
        String material = "";
        try {
            for (String string : config.getString("rewards").split(",")) {

                Matcher matcher = pattern.matcher(string);

                boolean matches = matcher.find();
                if (matches) {
                    material = matcher.group(1).trim();
                    int amount = Integer.parseInt(matcher.group(2));

                    if (material.equals("money")) {

                        rewards.add(new Rewards(amount));
                        continue;
                    }

                    ItemStack itemStack = itemFactory.createItemStack("minecraft:" + material);
                    itemStack.setAmount(amount);

                    rewards.add(new Rewards(itemStack));

                }
            }
            System.out.println("[SoulRewards] FileConfig created and loaded");

        } catch (NullPointerException | IllegalArgumentException e) {

            e.printStackTrace();

            instance.getLogger().log(java.util.logging.Level.SEVERE, "The rewards section in the config is empty, or not formatted correctly. Please check the config, if the materials have the same naming scheme as in minecraft.");
            instance.getLogger().log(java.util.logging.Level.SEVERE, material + " is not a valid material!");

        }

    }

    public static RewardsManager getRewardsManager() {
        return rewardsManager;
    }
}

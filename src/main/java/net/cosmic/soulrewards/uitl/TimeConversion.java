package net.cosmic.soulrewards.uitl;

public class TimeConversion {
    public static String timeConversion(long milisec) {
        int hours = (int) milisec / (1000 * 60 * 60);
        milisec -= hours * 60 * 60 * 1000;
        int minutes = (int) milisec / (1000 * 60);

        return hours + "h " + minutes + "m";
    }
}

package net.cosmic.soulrewards.uitl.BetterInventory;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public abstract class ItemCreator {


    /**
     * Creates an item with the given material, amount, displayname and lore
     *
     * @param material The material of the item
     * @param amount The amount of the item
     * @param displayname The name to be displayed
     * @param lore The lore to be displayed
     * @return ItemStack
     */
    public static ItemStack createItem(Material material, int amount, String displayname, String... lore) {

        ItemStack itemStack = new ItemStack(material);
        ItemMeta itemMeta = itemStack.getItemMeta();

        itemMeta.setDisplayName(displayname);
        itemMeta.setLore(Arrays.asList(lore));
        itemStack.setItemMeta(itemMeta);
        itemStack.setAmount(amount);

        return itemStack;
    }
}
package net.cosmic.soulrewards.uitl.BetterInventory;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.profile.PlayerProfile;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.UUID;

public abstract class SkullCreator {

    public static ItemStack createRealPlayerSkull(String displayName, UUID uuid, ChatColor color, String... lore){
        PlayerProfile playerProfile = Bukkit.createPlayerProfile(uuid, displayName);

        ItemStack skull = new ItemStack(Material.PLAYER_HEAD);
        SkullMeta sm = (SkullMeta) skull.getItemMeta();
        sm.setDisplayName(color + displayName);
        sm.setLore(Arrays.asList(lore));
        sm.setOwnerProfile(playerProfile);

        skull.setItemMeta(sm);

        return skull;
    }
}
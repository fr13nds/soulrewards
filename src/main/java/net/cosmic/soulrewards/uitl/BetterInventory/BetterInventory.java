package net.cosmic.soulrewards.uitl.BetterInventory;



import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.Comparator;
import java.util.UUID;

import static net.cosmic.soulrewards.uitl.BetterInventory.ItemCreator.createItem;
import static net.cosmic.soulrewards.uitl.BetterInventory.SkullCreator.createRealPlayerSkull;
import static net.cosmic.soulrewards.uitl.Util.fromMatrixToArray;


public class BetterInventory {

    public static ItemStack[][] menu = {
            {null, null, null, null, null, null, null, null, null},
            {null, null, createRealPlayerSkull("fr13nds_", UUID.fromString("0267baec-9fdd-471e-af6f-a8e72e40d5ea"), ChatColor.RED, "Framework Dev"), null, null , null, createRealPlayerSkull("4seenLP", UUID.fromString("509333b4-3607-4931-9460-a838bf4ed73c"), ChatColor.RED, "Framework Dev"), null, null},
            {null, null, null, null, null , null, null, null, null},
    };
    private final Inventory inventory;

    private boolean scrollable = false;

    // Standard: inventory 5x9 with size one border
    int size = 5*9;
    private boolean[] occupiedSlots = new boolean[size];
    {
        for(int i = 0; i < size; i++) {
            if (i < 10 || i > size - 9) {
                occupiedSlots[i] = true;
            }
            if (i % 9 == 0 || i % 9 == 8) {
                occupiedSlots[i] = true;
            }
        }
    }

    // Should never intervene with MENU_ITEMS
    private ItemStack[] ITEMS_TO_DISPLAY = null;

    // Should never intervene with ITEMS_THAT_SHOULD_BE_DISPLAYED
    private ItemStack[] MENU_ITEMS = null;

    // Standard Inventory without any parameters, is scrollable
    public BetterInventory(int size, String title) {
        if(size % 9 != 0) throw new IllegalArgumentException("Size must be a multiple of 9");

        inventory = Bukkit.createInventory(null, size, title);

        this.MENU_ITEMS = fromMatrixToArray(menu, menu.length, menu[0].length);

        scrollable = true;
    }

    // For a Hybrid of Menu and Items
    public BetterInventory(ItemStack[][] ITEMS_TO_DISPLAY, ItemStack[][] MENU_ITEMS, int size, String title, boolean scrollable) {
        if(size % 9 != 0) throw new IllegalArgumentException("Size must be a multiple of 9");

        inventory = Bukkit.createInventory(null, size, title);

        this.ITEMS_TO_DISPLAY = fromMatrixToArray(ITEMS_TO_DISPLAY, ITEMS_TO_DISPLAY.length, ITEMS_TO_DISPLAY[0].length);
        this.MENU_ITEMS = fromMatrixToArray(MENU_ITEMS, MENU_ITEMS.length, MENU_ITEMS[0].length);

        this.scrollable = scrollable;

        checkInterfere();
    }

    // Only for Menu Inventory
    public BetterInventory(ItemStack[][] MENU_ITEMS, int size, String title, boolean scrollable) {
        if(size % 9 != 0) throw new IllegalArgumentException("Size must be a multiple of 9");

        inventory = Bukkit.createInventory(null, size, title);

        this.MENU_ITEMS = fromMatrixToArray(MENU_ITEMS, MENU_ITEMS.length, MENU_ITEMS[0].length);

        this.scrollable = scrollable;
    }
    public void open(Player player) {
        player.openInventory(inventory);
    }

    /**
     * Updates the MenuItems
     * @param newMenu The new MenuItems
     */
    public void updateMenuItems(ItemStack[][] newMenu) {

        MENU_ITEMS = fromMatrixToArray(newMenu, newMenu.length, newMenu[0].length);
    }

    /**
     * Updates one MenuItem
     * @param newItem The new Item
     * @param x The x coordinate
     * @param y The y coordinate
     */
    public void updateMenuItem(ItemStack newItem, int x, int y) {
        MENU_ITEMS[x + y * 9] = newItem;
    }

    public void alphabeticalSort() {
        if(ITEMS_TO_DISPLAY == null) {
            throw new NullPointerException("Items are null, must be Set");
        }

        Arrays.stream(ITEMS_TO_DISPLAY).sorted(Comparator.comparing(itemStack -> itemStack.getItemMeta().getDisplayName()));
    }

    // CurrentLastItem stores last index of ITEMS_TO_DISPLAY that was displayed
    // Scrolling by overwriting items in inventory field
    private int currentLastItem = 0;
    public void scrollPages(){
        if (size != occupiedSlots.length || size != inventory.getSize()) {
            throw new UnsupportedOperationException("Inconsistent Sizes of inventory, occupiedSlots, size");
        }

        for (int i = currentLastItem; i < occupiedSlots.length; i++) {
            if (!occupiedSlots[i] || i > ITEMS_TO_DISPLAY.length) {
                inventory.setItem(i, ITEMS_TO_DISPLAY[i]);
                currentLastItem = i;
            }
        }

        // Call Method setting the Edge of the inventory
    }

    /**
     * Checks if the Items are interfering with each other
     * @throws IllegalArgumentException if the Items are interfering with each other
     */
    private void checkInterfere() throws IllegalArgumentException{

        for (ItemStack items : ITEMS_TO_DISPLAY) {
            for (ItemStack menu_item : MENU_ITEMS) {
                //Should be an exclusive or
                if(items == null && menu_item == null) continue;
                else if(items == null ^ menu_item == null) {
                    continue;
                }
                else throw new IllegalArgumentException();
            }
        }
    }

    /**
     * Fills the Inventory with the given Material
     * @param material The Material to fill the Inventory with
     * @return itself
     */
    public BetterInventory fillBackground(Material material){

        for(int i = 0; i < inventory.getSize(); i++){
            if(inventory.getItem(i) == null){
                inventory.setItem(i, createItem(material, 1, " ", (String) null));
            }
        }

        return this;
    }

    /**
     * Fills the Inventory with the given Menu
     * @return itself
     */
    public BetterInventory fillMenu(){
        inventory.setContents(MENU_ITEMS);

        return this;
    }

}


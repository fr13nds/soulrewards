package net.cosmic.soulrewards.uitl.MoneyManager;

import com.earth2me.essentials.Essentials;
import net.cosmic.soulrewards.SoulRewards;
import net.ess3.api.MaxMoneyException;
import org.bukkit.entity.Player;

import java.math.BigDecimal;

public class MoneyManager {
    Essentials essentials = SoulRewards.getInstance().getEssentials();

    public void addPurseBalance(Player player, int amount)  {

        try {
            essentials.getUser(player).giveMoney(new BigDecimal(amount));
            BigDecimal bigDecimal = essentials.getUser(player).getMoney();

            essentials.getUser(player).setMoney(new BigDecimal(bigDecimal.intValue() + amount));
        }catch (MaxMoneyException exception){
            player.sendMessage("You have reached the max money limit");
        }

    }
}

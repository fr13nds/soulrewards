package net.cosmic.soulrewards.uitl.FileConfigManager;

import net.cosmic.soulrewards.SoulRewards;
import org.bukkit.entity.Player;

public class FileManager {


    private final SoulRewards instance;
;

    public FileManager(SoulRewards instance){
        this.instance = instance;

    }

    public void addPlayer(Player player){
        instance.getFileConfig().set("players." + player.getUniqueId() + ".time", 0);

        instance.getFileConfig().saveConfig();
    }

    public boolean rewardAvailable(Player player){


        long timeUntilReward = getTimeUntilReward(player);

        return timeUntilReward <= 0;


    }

    public void resetTime(Player player){
        instance.getFileConfig().set("players." + player.getUniqueId() + ".time", getCurrentTime());

        instance.getFileConfig().saveConfig();
    }



    public boolean playerExists(Player player){
        return instance.getFileConfig().contains("players." + player.getUniqueId());
    }


    //Only for admins and debug testing
    public void setRewardsTimeZero(Player player){
        instance.getFileConfig().set("players." + player.getUniqueId() + ".time", 0);
        instance.getFileConfig().saveConfig();
    }

    public long getTimeUntilReward(Player player){

        return 86400000 - (getCurrentTime() - instance.getFileConfig().getLong("players." + player.getUniqueId() + ".time")) < 0 ? 0 : 86400000 - (getCurrentTime() - instance.getFileConfig().getLong("players." + player.getUniqueId() + ".time"));

    }

    private long getCurrentTime() {
        return  System.currentTimeMillis();
    }
}
